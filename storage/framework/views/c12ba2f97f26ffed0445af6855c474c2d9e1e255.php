<?php $__env->startSection('content'); ?>


    <!-- Start Header -->
    <!-- Preloader -->
    
    <!-- Start Slider section	 -->

    <section class=slider id=top>
        <!-- Start Slider container -->
        <div class=full-slider>
            <!-- Start Slide item -->
            <div class="slide vertical-align" data-image="<?php echo e(asset('public/img/mapaxl.jpg')); ?>">
                <div class="container-slide center">
                    <div class=container>
                        <div class="row">
                            <div id=mx>
                                <img src="<?php echo e(asset('public/img/mexico.png')); ?>" width="30%"alt="">
                                Mexico
                            </div>
                            <div id=pm>
                                Panama
                            </div>
                            <div class="conainer">
                                <div class="row">
                                    <div id=cl>
                                        Colombia
                                    </div>
                                    <div id=vz>
                                        Venezuela
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=loader>
                    <div class=loader_inner></div>
                </div>
            </div>
        </div>
    </section>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>