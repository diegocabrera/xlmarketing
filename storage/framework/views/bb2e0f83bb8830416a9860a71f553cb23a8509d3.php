<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <?php echo $__env->make('pagina::partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </head>
    <body class="dark">
        <div class="row">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
        <?php echo $__env->make('pagina::partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body>
</html>
