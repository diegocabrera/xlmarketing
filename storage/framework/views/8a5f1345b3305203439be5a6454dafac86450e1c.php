<?php $__env->startSection('content'); ?>
    <!-- Start Header -->
    <!-- Preloader -->
    <header id=top-nav>
        <div class=container>
            <div class=row>
                <!-- Logo -->
                <div id="caja">
                <a class="logo smooth-scroll" href="#top">
                    <img style="margin-top:5px;"src="<?php echo e(asset('public/img/logos/logo_80.png')); ?>" width="100%" alt="">
                </a>
                </div>
                <!-- Top navigation -->
                <nav class=top-menu>
                    <ul class="sf-menu clearfix">
                        <li><a href="#about" class=smooth-scroll>Somos...</a></li>
                        <li><a href="#works" class=smooth-scroll>Nuestro Trabajo</a></li>
                        <li><a href="#testimonials" class=smooth-scroll>Testimonios</a></li>
                        <li><a href="#contact" class=smooth-scroll>Contactos</a></li>
                    </ul>
                </nav>
                <!-- Toggle menu -->
                <a href="#" class=toggle-mnu>
                    <span></span>
                </a>
                <!-- Mobile menu -->
                <div id=mobile-menu>
                    <div class=inner-wrap>
                        <nav>
                            <ul class=nav_menu>

                                <li><a href="#about" class=smooth-scroll>Somos...</a></li>
                                <li><a href="#works" class=smooth-scroll>Nuestro Trabajo</a></li>
                                <li><a href="#testimonials" class=smooth-scroll>Testimonios</a></li>
                                <li><a href="#contact" class=smooth-scroll>Contactos</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- end mobile menu -->
            </div>
        </div>
    </header>
    <!-- Start Slider section	 -->
    <section class=slider id=top>
        <!-- Start Slider container -->
        <div class=full-slider>
            <!-- Start Slide item -->
            <div class="slide vertical-align bg-mask-black" data-image="<?php echo e(asset('public/img/XLIMG.jpg')); ?>">
                <div class="container-slide center">
                    <div class=container>
                        <div class=row>
                            <div class="col-md-12 content-mill">
                                <div class=content-slide>
                                    <div class=description>
                                    </div>
                                    <!-- heading title -->
                                    <center>
                                    <h1>
                                      <img src="<?php echo e(asset('public/img/logos/Logo_XL_web .png')); ?>" width="25%" alt="">
                                    </h1>
                                    <!-- horizontal line -->
                                    <span class=horizontal-line><span></span></span>
                                    <div class="btn-description tlt">
                                        <ul class="texts hidden">
                                            <li data-out-effect=fadeOut data-out-shuffle=true>Diseños Maravillosos</li>
                                            <li data-in-effect=fadeIn>Desarrollo Website intuitivo</li>
                                            <li data-in-effect=fadeIn>Puedeos crear cosas increíbles</li>
                                            <li data-in-effect=fadeIn>Para su empresa</li>
                                            <li data-in-effect=fadeIn>¡Trabajemos juntos!</li>
                                        </ul>
                                    </div>
                                    <div class=button-section>
                                        <a href="#contact" class="btn btn-blue smooth-scroll">¡Contactanos ahora!</a>
                                        <a href="#works" class="btn btn-dark smooth-scroll">Nustro Trabajo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </center>
                <!-- Canvas -->
                <div id=particles-js class=canvas-background></div>
                <!-- Loader -->
                <div class=loader>
                    <div class=loader_inner></div>
                </div>
            </div>
        </div>
        <!-- Strat Control slider -->
        <div class=control-slider>
            <!-- Pagiation container -->
            <div class=dots-control id=dots-control-full-slider></div>
            <!-- Prev - Next -->
            <div class=prev-next>
                <div class=prev><i class=pe-7s-angle-left></i></div>
                <div class=next><i class=pe-7s-angle-right></i></div>
            </div>
        </div>
    </section>
    <!-- Start socialite container -->
    <div class=socialite>
        <div class=container>
            <div class=row>
                <div class=container-socialite>
                    <div class="icon-down toBottomFromTop">
                        <a href="#about" class=smooth-scroll>
                            <i class=pe-7s-angle-down></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 item">
                    <a href="#"></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 item">
                    <a href="https://www.facebook.com/xlpanama/"><img src="public/img/s2.svg" alt=facebook></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 item">
                    <a href="https://www.instagram.com/xlpanama/"><img src="public/img/s1.svg" alt=instagram></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 item">
                    <a href="#"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Describe Your Best Section -->
    <section class=describe id=about>
        <div class=container>
            <div class=row>
                <!-- iphones -->
                <div class="col-md-7 col-md-push-5" class="row justify-around" style="top:70px">
                    <img src="<?php echo e(asset('public/img/desktop_XL.jpg')); ?>" class=img-responsive alt="photo" style="margin-bottom:70px">
                </div>
                <div class="col-md-5 col-md-pull-7">
                    <div class="heading-title text-left">
                        <h2>XL MARKETING GROUP</h2>
                        <div style="text-align:justify; margin-left:5px; margin-right:5px ;"><p style="font-size:14px;">​Somos una empresa de publicidad orientada a los resultados, con precios competitivos y garantía total de satisfaccion en todos los servicios y productos que brindamos.
                        </p></div>
                          <br>
                        <h2>MISIÓN</h2>
                        <div style="text-align:justify; margin-left:5px; margin-right:5px ;"><p style="font-size:14px;">Ofrecer servicios integrales de marketing con la mayor calidad eficiencia y eficacia, que nos permita contar con cliente satisfecho y buenos colaboradores comprometidos cada dia con su trabajo</p>
                        </div>
                          <br>
                        <h2>VISIÓN</h2>
                        <div style="text-align:justify; margin-left:5px; margin-right:5px ;"><p style="font-size:14px;">Seguir creciendo entre la competencia convirtiéndonos en la empresa líder en el desarrollo y ejecución de proyectos exitosos.</p>
                        </div>
                      </div>
                    </div>
            </div>
        </div>
    </section>
    <!-- Start Our Features section	 -->
    <section class="features bg-mask-black bg-image" data-image="img/bgn.jpg">
        <div class=container>
            <div class=row>
                <!-- Item -->
                <div class="col-md-3 col-sm-6 col-xs-12 item-icon">
                    <div class=icon-container>
                        <div class="icon icon-circle">
                            <i class=pe-7s-diamond></i>
                        </div>
                    </div>
                    <h3>Diseño Grafico</h3>
                </div>
                <!-- Item -->
                <div class="col-md-3 col-sm-6 col-xs-12 item-icon">
                    <div class=icon-container>
                        <div class="icon icon-circle">
                            <i class=pe-7s-phone></i>
                        </div>
                    </div>
                    <h3>Desarrollo Movil</h3>
                </div>
                <!-- Item -->
                <div class="col-md-3 col-sm-6 col-xs-12 item-icon">
                    <div class=icon-container>
                        <div class="icon icon-circle">
                            <i class=pe-7s-global></i>
                        </div>
                    </div>
                    <h3>Desarrollo Web</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 item-icon">
                    <div class=icon-container>
                        <div class="icon icon-circle">
                            <i class=pe-7s-graph1></i>
                        </div>
                    </div>
                    <h3>Marketing</h3>
                </div>

                <!-- Edn items -->
            </div>
    <div class="col">
    </div>
    <div class="col order-6">
        <p>Ten acceso inmediato a los mejores programadores de Apps, Website, diseñadores, </p>
          <p>y maestros en el Marketing de grandes y medianas empresas</p>
    </div>
    <div class="col order-8">
    </div>
  </div>
</div>
            <!-- Item -->
        </div>
    </section>
    <!-- Start Screenshots Section -->
    <section class=portfolio id=works>
        <div class=container>
            <div class=row>
                <div class="col-md-8 col-md-push-2">
                    <div class=heading-title>
                        <h2>Nuestro Trabajo</h2>
                    </div>
                </div>
            </div>
            <div class=row>
                <div class=controls-portfolio>
                    <ul id=control-portfolio class=text-center>
                        <li id=liact class="filter select-cat" data-filter="*">Todo</li>
                        <li class=filter data-filter=.category-1>Diseños</li>
                        <li class=filter data-filter=.category-3>Website</li>
                        <li class=filter data-filter=.category-4>Community Manager</li>
                    </ul>
                </div>
                <div id=portfolio>
                    <!-- Start Block item -->
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 mix category-1">
                        <a href="<?php echo e(asset('public/img/portafolio/1.jpg')); ?>" class=link-portfolio>
                            <div class=item-portfolio>
                                <div class=preview-container>
                                    <div class="button-preview icon-circle">
                                        <span class=preview><i class=pe-7s-search></i></span>
                                    </div>
                                </div>
                                <img src="<?php echo e(asset('public/img/portafolio/1.jpg')); ?>" alt=screnshot>
                            </div>
                        </a>
                    </div>
                    <!-- Block item -->
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 mix category-1">
                        <a href="<?php echo e(asset('public/img/portafolio/2.jpg')); ?>" class=link-portfolio>
                            <div class=item-portfolio>
                                <div class=preview-container>
                                    <div class="button-preview icon-circle">
                                        <span class=preview><i class=pe-7s-search></i></span>
                                    </div>
                                </div>
                                <img src="<?php echo e(asset('public/img/portafolio/2.jpg')); ?>" alt=screnshot>
                            </div>
                        </a>
                    </div>
                    <!-- Block item -->
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 mix category-1">
                        <a href="<?php echo e(asset('public/img/portafolio/3.jpg')); ?>" class=link-portfolio>
                            <div class=item-portfolio>
                                <div class=preview-container>
                                    <div class="button-preview icon-circle">
                                        <span class=preview><i class=pe-7s-search></i></span>
                                    </div>
                                </div>
                                <img src="<?php echo e(asset('public/img/portafolio/3.jpg')); ?>" alt=screnshot>
                            </div>
                        </a>
                    </div>
                    <!-- Block item -->
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 mix category-4">
                        <a href="<?php echo e(asset('public/img/portafolio/1_1.jpg')); ?>" class=link-portfolio>
                            <div class=item-portfolio>
                                <div class=preview-container>
                                    <div class="button-preview icon-circle">
                                        <span class=preview><i class=pe-7s-search></i></span>
                                    </div>
                                </div>
                                <img src="<?php echo e(asset('public/img/portafolio/1_1.jpg')); ?>" alt=screnshot>
                            </div>
                        </a>
                    </div>
                    <!-- Block item -->
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 mix category-4">
                        <a href="<?php echo e(asset('public/img/portafolio/1_2.jpg')); ?>" class=link-portfolio>
                            <div class=item-portfolio>
                                <div class=preview-container>
                                    <div class="button-preview icon-circle">
                                        <span class=preview><i class=pe-7s-search></i></span>
                                    </div>
                                </div>
                                <img src="<?php echo e(asset('public/img/portafolio/1_2.jpg')); ?>" alt=screnshot>
                            </div>
                        </a>
                    </div>
                    <!-- Block item -->
                    
                    <!-- Block item -->
                    <!-- End Block item -->
                </div>
                <!-- Edn div screenshot`s -->
                <!-- End Control slider -->
            </div>
        </div>
    </section>
    <!-- Start Testimonials Section -->
    
    <!-- Start Contact Section -->
    <section class="contact-info bg-mask-black bg-image">
        <div class=container>
            <div class=row>
                <div class="col-md-10 col-md-push-1">
                    <!-- Block item -->
                    <div class="col-md-4 col-sm-4 contacn-block">
                        <div class="section-class-item icon-circle-border">
                            <div class=item-icon>
                                <span class="icon-wrap icon-services"><i class="fa fa-phone" aria-hidden=true></i></span>
                            </div>
                            <div class="item-text text-center">
                                <h3 style="color:white;">Telefonos</h3>
                                <p style="color:white;">Panama</p>
                                <p style="color:white;">(+507) 236-4215</p>
                                <span class=horizontal-line><span></span></span>
                                <p style="color:white;">Venezuela</p>
                                <p style="color:white;">(+58) 285 63401680</p>
                            </div>
                        </div>
                    </div>
                    <!-- Block item -->
                    <div class="col-md-4 col-sm-4 contacn-block">
                        <div class="section-class-item icon-circle-border">
                            <div class=item-icon>
                                <span class="icon-wrap icon-services"><i class="fa fa-envelope-o" aria-hidden=true></i></span>
                            </div>
                            <div class="item-text text-center">
                                <h3 style="color:white;">Email</h3>
                                <p style="color:white;">info@grupoxlmarketing.com</p>
                            </div>
                        </div>
                    </div>
                    <!-- Block item -->
                    <div class="col-md-4 col-sm-4 contacn-block">
                        <div class="section-class-item icon-circle-border">
                            <div class=item-icon>
                                <span class="icon-wrap icon-services"><i class="fa fa-map-marker" aria-hidden=true></i></span>
                            </div>
                            <div class="item-text text-center">
                                <h3 style="color:white;">Direccion</h3>
                                <p style="color:white;">Panama</p>
                                <p style="color:white;">Torre Delta, piso 9 oficina 902, Bella Vista, Ciudad de Panamá, Panamá.</p>
                                <span class=horizontal-line><span></span></span>
                                <p style="color:white;">Venezuela</p>
                                <p style="color:white;"> C.C. Samara, piso 1 oficina 24,Ciudad Bolivar, Estado Bolivar, Venezuela.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Block item -->
                </div>
            </div>
        </div>
    </section>
    <!-- Start Contact Form Section -->
    <section class="form-contact bg-mask-black bg-image" data-image="img/bg2.jpg" id=contact>
        <div class=container>
            <div class=row>
                <div class="col-md-8 col-md-push-2">
                    <div class=heading-title>
                        <h2>Contactenos</h2>
                    </div>
                </div>
            </div>
            <div class=row>
                <div class=contact-form>
                    <form id=contact-form class=contact-form>
                        <!-- Input name -->
                        <div class=col-md-4>
                            <p>
                                <input type=text required="" name=name class=form-control placeholder="Nombre Completo">
                            </p>
                        </div>
                        <!-- Input email -->
                        <div class=col-md-4>
                            <p class=contact-form-email>
                                <input type=email required="" name=email class=form-control placeholder="Email">
                            </p>
                        </div>
                        <!-- Input website -->
                        <div class=col-md-4>
                            <p class=contact-form-email>
                                <input type=text name=website class=form-control placeholder="Sitio WEB">
                            </p>
                        </div>
                        <!-- textarea -->
                        <div class=col-md-12>
                            <p class=contact-form-message>
                                <textarea rows=9 required="" name=message class=form-control placeholder="Mensaje"></textarea>
                            </p>
                            <p id=success class="hidden notify">Su mensaje se envio con exito</p>
                            <p id=error class="hidden notify">Error al enviar mensaje</p>
                            <p class=contact-form-submit>
                                <input type=submit value="Enviar" class="btn btn-blue btn-form">
                            </p>
                        </div>
                    </form>
                </div>
                <!-- end contact section -->
            </div>
        </div>
    </section>
    <!-- Start footer -->
    <footer>
        <div class=container>
            <div class=row>
                <div class=col-md-12>
                    <div class=heading-title>
                        <h2><span>XL<span>Marketing</span></span>
                        </h2>
                    </div>
                    <div class=social-inons>
                        <ul>
                            <li class=icon-circle><a href="https://www.facebook.com/xlpanama/"><i class="fa fa-facebook" aria-hidden=true></i></a></li>
                            <li class=icon-circle><a href="https://www.instagram.com/xlpanama/"><i class="fa fa-instagram" aria-hidden=true></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=row>
                <div class="bottom-footer white-color">
                    <div class=col-md-6>
                        <div class=footer-copyright>
                            <p>Xl Marketing &copy; 2018 All Rights Reserved. Developed by - <a href="#s" target=_blank>Group XL Marketing</a></p>
                        </div>
                    </div>
                    <div class=col-md-6>
                        <!-- Bottom nav -->
                        <div class=footer-menu>
                            <ul>
                                <li><a href="#">Terms</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Licenses</a></li>
                            </ul>
                        </div>
                        <!-- End Bottom nav -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Scroll to top -->
    <div class="top icon-down toTopFromBottom">
        <a href="#top" class=smooth-scroll><i class=pe-7s-angle-up></i></a>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>