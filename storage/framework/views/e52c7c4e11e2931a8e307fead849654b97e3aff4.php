	
	<!-- END PRE-FOOTER -->
	<!-- BEGIN INNER FOOTER -->
	<div class="page-footer">
		<div class="container-fluid"> <?php echo e(date('Y')); ?> &copy; <?php echo e($controller->conf('nombre_empresa')); ?>.
		</div>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>