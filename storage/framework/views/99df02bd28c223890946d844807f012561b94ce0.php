<?php $__env->startSection('content'); ?>


    <!-- Start Header -->
    <!-- Preloader -->
    
    <!-- Start Slider section	 -->
    <section class=slider id=top>
        <!-- Start Slider container -->
        <div class=full-slider>
            <!-- Start Slide item -->
            <div class="slide vertical-align bg-mask-black" data-image="<?php echo e(asset('public/img/bgy.jpg')); ?>">
                <div class="container-slide center">
                    <div class=container>
                        <div class=row>
                            <div class="col-md-12 content-mill">
                                <div class=content-slide>
                                    <div class=description>
                                        <div class="col-md-12 col-xs-12 col-xl-12">
                                            <div class="col-lg-2 col-md-2 col-xs-6 col-md-offset-5 col-lg-offset-5 col-xs-offset-3">
                                                <center>
                                                    <img width="100%" src="<?php echo e(asset('public/img/logos/login_logo.png')); ?>" alt="">
                                                </center>
                                                <br>
                                            </div>
                                            <br><br>
                                        </div>
                                    </div>
                                    <!-- heading title -->
                                    <h1 class="heading-title-big ">
                                        Coming Soon
                                    </h1>
                                    <!-- horizontal line -->
                                    <span class=horizontal-line><span></span></span>
                                    <div class="btn-description tlt">
                                        <ul class="texts hidden">
                                            <li data-out-effect=fadeOut data-out-shuffle=true>Ui/UX Desingners</li>
                                            <li data-in-effect=fadeIn>Front-End & Back-End Developer</li>
                                            <li data-in-effect=fadeIn>I Can Create Awesome Stuff</li>
                                            <li data-in-effect=fadeIn>For Your Business</li>
                                            <li data-in-effect=fadeIn>Let's work together</li>
                                        </ul>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Canvas -->
                <div id=particles-js class=canvas-background></div>
                <!-- Loader -->
                <div class=loader>
                    <div class=loader_inner></div>
                </div>
            </div>
        </div>
        <!-- Strat Control slider -->
        <div class=control-slider>
            <!-- Pagiation container -->
            <div class=dots-control id=dots-control-full-slider></div>
            <!-- Prev - Next -->
            <div class=prev-next>
                <div class=prev><i class=pe-7s-angle-left></i></div>
                <div class=next><i class=pe-7s-angle-right></i></div>
            </div>
        </div>
    </section>

    

    <!-- Scroll to top -->
    <div class="top icon-down toTopFromBottom">
        <a href="#top" class=smooth-scroll><i class=pe-7s-angle-up></i></a>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>