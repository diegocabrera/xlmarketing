<?php

Route::group(['middleware' => 'web', 'prefix' => 'testimonio', 'namespace' => 'App\\Modules\Testimonio\Http\Controllers'], function()
{
    Route::get('/', 'TestimonioController@index');
});
