<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix').'/portafolio', 'namespace' => 'App\\Modules\Portafolio\Http\Controllers'], function()
{
    Route::get('/', 'PortafolioController@index');
});
