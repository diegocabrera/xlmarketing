<?php
namespace App\Modules\Portafolio\Models;

use App\Modules\Base\Models\Modelo;
use Carbon\Carbon;

class Portafolio extends modelo
{
    protected $table = 'galeria';
    protected $fillable = ['id', 'nombre', 'slug','categoria_id', 'published_at'];

    public function imagenes()
	{
        return $this->hasMany('App\Modules\Portafolio\Models\PortafolioImagenes');
    }

}
