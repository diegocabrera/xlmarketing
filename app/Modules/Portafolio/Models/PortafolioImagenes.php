<?php
namespace App\Modules\Portafolio\Models;

use App\Modules\Base\Models\Modelo;


class PortafolioImagenes extends modelo
{
    protected $table = 'portafolio_imagenes';
    protected $fillable = ["portafolio_id","archivo","descripcion","leyenda","tamano"];
    protected $campos = [
        'archivo' => [
            'type' => 'text',
            'label' => 'Archivo',
            'placeholder' => 'Archivo del Galeria Imagenes'
        ],
        'descripcion' => [
            'type' => 'text',
            'label' => 'Descripcion',
            'placeholder' => 'Descripcion del Galeria Imagenes'
        ],
        'leyenda' => [
            'type' => 'text',
            'label' => 'Leyenda',
            'placeholder' => 'Leyenda del Galeria Imagenes'
        ],
        'tamano' => [
            'type' => 'text',
            'label' => 'Tamano',
            'placeholder' => 'Tamano del Galeria Imagenes'
        ]
    ];

    public function portafolio()
	{
        return $this->belongsTo('App\Modules\Portafolio\Models\Portafolio');
    }
}
