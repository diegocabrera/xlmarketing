<?php
    $menu['portafolio']= [
        [
            'nombre' => 'Portafolio',
            'direccion' => '#Portafolio',
            'icono' => 'fa fa-eye',
            'menu' => [
                [
                    'nombre' => 'Portafolio',
                    'direccion' => 'portafolio',
                    'icono' => 'fa fa-picture-o'
                ],
                [
                    'nombre' => 'Categorias',
                    'direccion' => 'categorias',
                    'icono' => 'fa fa-volume-up '
                ],
            ]
        ]
    ];
 ?>
