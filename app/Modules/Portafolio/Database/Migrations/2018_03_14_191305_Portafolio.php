<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portafolio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table)
        {
            $table->increments('id');
      			$table->string('nombre', 250);
      			$table->string('slug', 250)->unique();
      			$table->text('descripcion', 250);

      			$table->timestamps();
      			$table->softDeletes();
        });

        Schema::create('portafolio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 200);
            $table->integer('categoria_id')->unsigned();
            $table->timestamp('published_at')->nullable();
            $table->string('slug', 250)->unique();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('categoria_id')
                ->references('id')
                    ->on('categorias')
                        ->onDelete('cascade')
                            ->onUpdate('cascade');
        });

        Schema::create('portafolio_imagenes', function (Blueprint $table) {

          	$table->increments('id');
          	$table->integer('portafolio_id')->unsigned();
          	$table->string('archivo', 200);
          	$table->string('descripcion', 200);
          	$table->string('leyenda', 200);
          	$table->string('tamano', 12);

          	$table->foreign('portafolio_id')
          		->references('id')->on('portafolio')
          		->onDelete('cascade')->onUpdate('cascade');

          	$table->timestamps();
			      $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galeria_imagenes');
        Schema::drop('portafolio');
        Schema::drop('categorias');
    }
}
