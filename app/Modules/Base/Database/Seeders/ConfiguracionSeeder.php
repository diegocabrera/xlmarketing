<?php

namespace App\Modules\Base\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modules\Base\Models\Configuracion;

class ConfiguracionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$configuraciones = [
			'logo'           => 'logo.png',
			'login_logo'     => 'login_logo.png',
			'nombre'         => 'XL Marketing',
			'formato_fecha'  => 'd/m/Y',
			'miles'          => '.',
			'email'          => 'xl.programadores@grupoxlmarketing.com',
			'email_name'     => 'Base',
			'nombre_empresa' => 'XL Marketing'
    	];

    	foreach ($configuraciones as $propiedad => $valor) {
	        Configuracion::create([
				'propiedad' => $propiedad,
				'valor' => $valor
			]);
    	}
    }
}
