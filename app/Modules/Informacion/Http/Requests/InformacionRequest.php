<?php

namespace App\Modules\Informacion\Http\Requests;

use App\Http\Requests\Request;

class InformacionRequest extends Request {
    protected $reglasArr = [
		'mision' => ['required', 'min:3', 'max:250'], 
		'vision' => ['required', 'min:3', 'max:250'], 
		'resena' => ['min:3', 'max:250']
	];
}