<?php

namespace App\Modules\Informacion\Http\Controllers;

//Controlador Padre
use App\Modules\Informacion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Informacion\Http\Requests\InformacionRequest;

//Modelos
use App\Modules\Informacion\Models\Informacion;

class InformacionController extends Controller
{
    protected $titulo = 'Informacion';

    public $js = [
        'Informacion'
    ];
    
    public $css = [
        'Informacion'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('informacion::Informacion', [
            'Informacion' => new Informacion()
        ]);
    }

    public function nuevo()
    {
        $Informacion = new Informacion();
        return $this->view('informacion::Informacion', [
            'layouts' => 'base::layouts.popup',
            'Informacion' => $Informacion
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Informacion = Informacion::find($id);
        return $this->view('informacion::Informacion', [
            'layouts' => 'base::layouts.popup',
            'Informacion' => $Informacion
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Informacion = Informacion::withTrashed()->find($id);
        } else {
            $Informacion = Informacion::find($id);
        }

        if ($Informacion) {
            return array_merge($Informacion->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(InformacionRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Informacion = $id == 0 ? new Informacion() : Informacion::find($id);

            $Informacion->fill($request->all());
            $Informacion->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Informacion->id,
            'texto' => $Informacion->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Informacion::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Informacion::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Informacion::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Informacion::select([
            'id', 'mision', 'vision', 'resena', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}