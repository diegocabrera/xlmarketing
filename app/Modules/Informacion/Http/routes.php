<?php

Route::group(['middleware' => 'web', 'prefix' => 'informacion', 'namespace' => 'App\\Modules\Informacion\Http\Controllers'], function()
{
    Route::get('/', 'InformacionController@index');

    //{{route}}
});
