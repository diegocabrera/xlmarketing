<?php

namespace App\Modules\Informacion\Models;

use App\Modules\Base\Models\Modelo;



class Informacion extends Modelo
{
    protected $table = 'informacion';
    protected $fillable = ["mision","vision","resena"];
    protected $campos = [
    'mision' => [
        'type' => 'text',
        'label' => 'Mision',
        'placeholder' => 'Mision del Informacion'
    ],
    'vision' => [
        'type' => 'text',
        'label' => 'Vision',
        'placeholder' => 'Vision del Informacion'
    ],
    'resena' => [
        'type' => 'text',
        'label' => 'Rese\u00f1a',
        'placeholder' => 'Rese\u00f1a de la Informacion'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}