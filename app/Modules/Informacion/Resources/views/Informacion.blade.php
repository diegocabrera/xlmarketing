@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Informacion']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Informacion.',
        'columnas' => [
            'Mision' => '33.333333333333',
		'Vision' => '33.333333333333',
		'Reseña' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Informacion->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection