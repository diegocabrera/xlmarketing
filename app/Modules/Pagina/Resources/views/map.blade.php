
{{-- Script de la vista, para generar el mapa en ella --}}
<script>
    generarMapa();


    function generarMapa() {

      /*  $(function () {

        $(document).on('click','.mapa',function(){
        console.log("$municipios");
    });

        $(".mapa").click(function(){
        console.log("$municipios");
    });*/



    // Initiate the chart
    Highcharts.mapChart('mapa', {
        'chart': {
            'borderWidth': 1,
            'animation':false
        },

         'title': {
                'align': 'center',
                'text': '',
                'useHTML': false,
            },

         'legend': {
                'layout': 'vertical',
                'align': 'left',
                'verticalAlign': 'bottom'
            },


        'mapNavigation': {
            'enabled': true,
            'buttonOptions': {
                'verticalAlign': 'bottom'
            }
        },

        'tooltip': {
            'backgroundColor': 'none',
            'borderWidth': 0,
            'shadow': false,
            'useHTML': true,
            'padding': 0,
            pointFormat: '<span class="f32"><span class="flag {point.flag}">' +
                '</span></span> {point.name}<br>' +
                '<span style="font-size:30px">{point.value}/km²</span>',
            positioner: function () {
                return { x: 0, y: 250 };
            }
        },

        'colorAxis': {},

        'series': [{
                'type': 'map',
                'name': 'centro',
                'animation': {
                    'duration': 1000
                },
                'data': {!! json_encode($municipios) !!},
                'mapData': Highcharts.maps['venezuela/bolivar'],
                'joinBy': ['slug', 'slug'],
                'events': {
                    'click': function (e){
                                     $municipio = e.point.name;
                                        alert (e.point.name);
                                 }
                },

                'dataLabels': {
                    'enabled': true,
                    'color': '#a8e0ff',
                    'formatter': function() {
                        if (this.point.value > 0) {
                            //return this.key + " (" + this.point.value + ")";
                            return this.key;
                        }
                    }
                },
                'states': {
                    'hover': {
                        'color': '#1A3C82'
                    }

      }
            }]
        });
    }

</script>
