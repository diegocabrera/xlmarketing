<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        @include('pagina::partials.head')
    </head>
    <body class="dark">
        <div class="row">
            @yield('content')
        </div>
        @include('pagina::partials.footer')
    </body>
</html>
