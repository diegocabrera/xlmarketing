@extends('pagina::layouts.master')

@section('content')


    <!-- Start Header -->
    <!-- Preloader -->
    {{-- <header id=top-nav>
        <div class=container>
            <div class=row>
                <!-- Logo -->
                <a class="logo smooth-scroll" href="#top">XL<span> Marketing</span></a>
                <!-- Top navigation -->
                <nav class=top-menu>
                    <ul class="sf-menu clearfix">
                        <li><a href="#about" class=smooth-scroll>About</a></li>
                        <li><a href="#works" class=smooth-scroll>Works</a></li>
                        <li><a href="#testimonials" class=smooth-scroll>Testimonials</a></li>
                        <li><a href=blog.html class=smooth-scroll>Blog</a></li>
                        <li><a href="#contact" class=smooth-scroll>Contact</a></li>
                    </ul>
                </nav>
                <!-- Toggle menu -->
                <a href="#" class=toggle-mnu>
                    <span></span>
                </a>
                <!-- Mobile menu -->
                <div id=mobile-menu>
                    <div class=inner-wrap>
                        <nav>
                            <ul class=nav_menu>

                                <li><a href="#about" class=smooth-scroll>About me</a></li>
                                <li><a href="#works" class=smooth-scroll>My works</a></li>
                                <li><a href="#testimonials" class=smooth-scroll>Testimonials</a></li>
                                <li><a href=blog.html class=smooth-scroll>Blog</a></li>
                                <li><a href="#contact" class=smooth-scroll>Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- end mobile menu -->
            </div>
        </div>
    </header> --}}
    <!-- Start Slider section	 -->

    <section class=slider id=top>
        <!-- Start Slider container -->
        <div class=full-slider>
            <!-- Start Slide item -->
            <div class="slide vertical-align" data-image="{{asset('public/img/mapaxl.jpg')}}">
                <div class="container-slide center">
                    <div class=container>
                        <div class="row">
                            <div id=mx>
                                <img src="{{asset('public/img/mexico.png')}}" width="30%"alt="">
                                Mexico
                            </div>
                            <div id=pm>
                                Panama
                            </div>
                            <div class="conainer">
                                <div class="row">
                                    <div id=cl>
                                        Colombia
                                    </div>
                                    <div id=vz>
                                        Venezuela
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=loader>
                    <div class=loader_inner></div>
                </div>
            </div>
        </div>
    </section>



@stop
