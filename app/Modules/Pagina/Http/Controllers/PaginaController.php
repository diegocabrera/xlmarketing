<?php

namespace App\Modules\Pagina\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Modules\Pagina\Http\Controllers;
use Carbon\Carbon;
use DB;
use Mail;

class PaginaController extends Controller
{
    public $titulo = 'XL Marketing';

    public $css = [
        'main2',
        'main',
    ];

    public $js = [
        'libs',
        'commons'
    ];

    public function index()
    {
        return $this->view('pagina::index3');

    }

    public function email($r)
    {
        Mail::send("base::emails.solicitud", [
                        'correo' => $correo
                        ], function($message) use($usuario) {
                        $message->from('tributosbolivar8001@gmail.com', 'Bienvenido al Sistema de Tributos Bolívar');
                        $message->to($usuario->usuario, $usuario->personas(['nombres']))->subject("Bienvenido a Tributos Bolívar");
        });
    }
    public function index_dos()
    {
        return $this->view('pagina::index');
    }


}
