<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\\Modules\Pagina\Http\Controllers'], function()
{
    Route::get('/', 'PaginaController@index');
    Route::get('/index_dos', 'PaginaController@index_dos');
});
